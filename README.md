# Salesforce Metadata Backup Pipelines

#### Automate metadata backups from Salesforce using Salesforce DX and CI (Continuous Integration) Pipelines to a Git repository

This is a guide to help you configure your own automated metadata backups from a Salesforce org to a Git repository. This guide assumes that you are generally familiar with the principles of Git and source control processes.

Much of this guide is culled from several different Salesforce help documents, as having them in a single easy-to-read place is more useful than searching across multiple documents.

### Dependencies
This guide includes instructions that rely on the dependencies listed. Windows users should refer to the alternative tools called out below. Linux and macOS users can install `openssl` and `gpg` using their preferred package manager in a terminal.

- [Salesforce DX CLI](https://developer.salesforce.com/tools/sfdxcli)
- [Visual Studio Code](https://code.visualstudio.com/)
- [Salesforce Extension Pack](https://marketplace.visualstudio.com/items?itemName=salesforce.salesforcedx-vscode) for Visual Studio Code
- [Java SDK 8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- openssl (Windows: [64-bit](https://slproweb.com/download/Win64OpenSSL-1_1_1c.exe), [32-bit](https://slproweb.com/download/Win32OpenSSL-1_1_1c.exe))
- gpg (Windows: [GnuPG](https://gnupg.org/download/))

### Instructions
1. Create a new Git respository
2. Create a new Salesforce DX project in the local repository
3. Create a Salesforce Connected App to use with our CI Pipeline
     - Generate Certificate and Key using OpenSSL
     - Create a new Connected App
4. Encrypt a key with `gpg` or GnuPG
5. Build a `package.xml` manifest file
6. Configure a `pipeline.yaml` file
7. Commit to the Git repository
8. Set up Environment Variables
9. Turn on the Pipeline


___

## 1. Create a new Git repository
A Git repository is what allows us to track the metadata differences every time we back up Salesforce metadata. We can leverage this to roll metadata back to a point-in-time, or revert certain parts of the metadata like profiles or an individual field on an object.

I prefer using Bitbucket for free private repositories, but the principles in this guide should work with any Git repository provider that supports continuous integration pipelines and environment variables within those pipelines.  

Once you've established your repository, `git clone` it your local machine. If you don't like using the command line for managing Git repositories, you can use the GUI-based tools from [Bitbucket](https://www.sourcetreeapp.com/), [GitHub](https://desktop.github.com/), and other providers.

Ensure that your repository is private and can't be forked, unless you want people to be able to look at your metadata. 

---
## 2. Create a new Salesforce DX project in the local repository
In Visual Studio Code, you can use the Salesforce Extension Pack to spin up a blank **Salesforce DX project with manifest**. When prompted to select a folder, you should save this into the folder on your local machine that we just cloned from the Git repository.

---

## 3. Create a Salesforce Connected App to use with our CI Pipeline

Original documentation: 

- [Create a Connected App](https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_auth_connected_app.htm)
- [Authorize an Org Using the JWT-Based Flow](https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_auth_jwt_flow.htm)

Salesforce DX can use JWT (JSON Web Tokens)-based authorization to create a secure connection to your Salesforce org. However, this requires creating a Connected App in the Salesforce org that you wish to create backups from. 

Additionally, the whole JWT-based authorization flow requires first generating a digital certificate to use when creating a connected app. 

For each org that you want to backup, you'll execute these tasks only once. After that, you can authorize the org in a script that runs in your CI environment.

#### Generate Certificate and Key using OpenSSL
This process produces two files:

- `server.key` - The private key. You specify this file when you authorize an org with the `force:auth:jwt:grant` command in Salesforce DX.
- `server.crt` - The digital certification. You upload this file when you create a Connected App required by the JWT-based flow.

I recommend working in a new folder outside of the Git repo for this work.  

1. Use OpenSSL to generate a private key and store it in a file called `server.key`.  

    `openssl genrsa -des3 -passout pass:x -out server.pass.key 2048` 

    `openssl rsa -passin pass:x -in server.pass.key -out server.key`

2. Generate a certificate signing request using the server.key file. Store the certificate signing request in a file called `server.csr`. Enter some information about your 'company' when prompted - it doesn't have to be real information.  

    `openssl req -new -key server.key -out server.csr` 

3. Generate a self-signed digital certificate from the `server.key` and `server.csr` files. Store the certificate in a file called `server.crt`.

`openssl x509 -req -sha256 -days 365 -in server.csr -signkey server.key -out server.crt` 

You can delete the `server.pass.key` file because you will no longer need it.

#### Create a new Connected App

You can create a Connected App using the Setup menu in your Salesforce org. These steps assume that you are using Lightning Experience.

1. From Setup, enter `App Manager` in the Quick Find box to get to the Lightning Experience App Manager.
2. In the top-right corner, click **New Connected App**.
3. Update the basic information as needed, such as the connected app name and your email address.
4. Check the box for **Enable OAuth Settings**.
5. In the **Callback URL** field, you can use `http://localhost:1717/OauthRedirect`. Port 1717 is usually not an issue for continuous integration pipelines, but your mileage may vary.

  **Note:** this sample repository contains a `sfdx-project.json` file that is configured for port 1717. If you do change ports, be sure to update the `sfdx-project.json` file accordingly.

6. Check the box for **Use digital signatures**
7. Click Choose File and upload the `server.crt` file that contains your digital certificate, created in the "Generate Certificates using OpenSSL" section of this guide.
8. Add these OAuth scopes:
    - Access and manage your data (api)
    - Perform requests on your behalf at any time (refresh_token, offline_access)
    - Provide access to your data via the Web (web)
9. Leave **Require Secret for Web Server Flow** checked. Leave any remaining fields blank.
10. Click the **Save** button. 

***Make note of the consumer key, because you will need it to add it as an environment variable in your CI pipeline.***

11. Click the **Manage** button. 
12. Click **Edit Policies.**
13. In the *OAuth Policies* section, select **Admin approved users are pre-authorized for Permitted Users**, and click OK.
14. Click **Save**.
15. Click **Manage Profiles** and then click **Manage Permission Sets**. Select the profiles and permission sets that are pre-authorized to use this connected app. Create permission sets if necessary. The way you want to secure this is up to you, but I'd strongly recommend limiting access to the Connected App to only one user. 

## 4. Encrypt the Key using `gpg` or GnuPG
Now that we have a key to use for our backup work, we need to keep it secure -- anyone that has the key could potentially access the Salesforce org. 

Start a terminal session and navigate to the folder you were using to generate the certificate and key. 

Using `gpg` or GnuPG: `gpg -c server.key`

You'll be prompted to enter a passphrase to use for symmetric encryption. Make it a good one. It can be as long and complicated as you want, as it won't be able to be accessed directly as part of the repository. **Make note of the consumer key, because you will need it to add it as an environment variable in your CI pipeline.**

After executing the above command, you should now have a file named `server.key.gpg` or similar. Place the `server.key.gpg` file in the `manifest/` directory in your Salesforce DX project folder.

---

## 5. Build a `package.xml` manifest file
When working with Salesforce metadata, a `package.xml` manifest file defines the source code you're attempting to deploy or, in our case, retrieve from a Salesforce org. 

In Visual Studio Code, you can access the `package.xml` file from the `manifest/` directory in your Salesforce DX project. 

Salesforce has documentation on what's available to include in a `package.xml` manifest file, the exceptions, and some sample files for reference:

- [Metadata Components and Types](https://developer.salesforce.com/docs/atlas.en-us.api_meta.meta/api_meta/meta_objects_intro.htm)
- [Unsupported Metadata Types](https://developer.salesforce.com/docs/atlas.en-us.api_meta.meta/api_meta/meta_unsupported_types.htm)
- [Sample package.xml Manifest Files](https://developer.salesforce.com/docs/atlas.en-us.api_meta.meta/api_meta/manifest_samples.htm)

This repository already contains a `package.xml` file for reference as well, which is a manifest for:

- Most commonly used Standard objects 
- Namespaced managed objects from **EDA** (Education Data Architecture) and **NPSP** (Non-Profit Success Pack)
- All fields from the above listing of objects

If you simply wish to backup everything in your org, use the [Package Builder](https://packagebuilder.herokuapp.com/) Heroku app to build a comprehensive listing of everything in XML format, which you can copy/paste into your Salesforce DX project's `package.xml` file.

**Note:** Over time, parts of your `package.xml` manifest may become outdated as new pieces of metadata are added and removed from your org. Be sure to regularly review and update your manifest. 

--- 

## 6. Configure a `pipeline.yaml` file

Before configuring our continuous integration (CI) or continuous delivery (CD) pipeline, let's define what it is. 

A CI/CD pipeline helps *automate steps in your software delivery process*. Typically, this includes compiling code changes into new software 'builds', automating testing, and deploying to different environments.

This guide leverages the automated part of pipelines to do something a bit different, by simply backing up a current state of code (read: metadata) from Salesforce to a repository. 

The requirements for how to make your repository service play nicely with your `pipeline.yaml` file vary, but the principle is the same. This repository contains a sample file that you can use as a starting point.

The sample pipeline:
- leverages secure environment variables stored in your repository (prefixed in the file by a `$`) -- more on this later in the guide.
- runs its steps in a containerized environment, so it's not really publicly accessible outside of the repository.

Some possible configuration tweaks to make, depending on your use case:
- The sample file looks for a Sandbox org to authorize against. Change the reference to `https://test.salesforce.com` to `https://login.salesforce.com` if you need to backup a Production instance.
- Step 6 of the sample pipeline logs a single line with today's date to a `change.txt` file and adds or updates it in your repository. You can adjust this for more robust logging, or bring in additional tools to handle whatever notification processes you wish to support.
- The pipeline runs in a Node.js container, but you can adjust this to use any number of containerized images as supported by your repository host. Just pick one that works with Salesforce DX CLI :)

Use this as a starting point, or define your own steps. Save your `pipeline.yaml` file in the top-level folder of your repository local copy.

---

## 7. Commit to the Git repository

Now that we have our manifest file defined, we can commit and push the Salesforce DX project to the Git repository.

Use the GUI tool of your choice or the command line to `git commit` and `git push` to the **master** branch of your repository.

---

## 8. Set up Environment Variables

The `sample-pipeline.yaml` included in this repository calls on 4 environment variables that need to be setup within your repository's pipeline settings. 

`$USERNAME` A user in the Salesforce org with permission to access the Connected App.

`$DEV_SECRET` Consumer key from the Connected App (Step 3).

`$PASSPHRASE` The passphrase used for encrypting the `server.key` (Step 4).

`$SFDXORGALIAS` A friendly name for the connection to your Salesforce org.

---

## 9. Turn on the Pipeline!

Again, this step can vary widely depending on where you're hosting your repository. You can use your `pipeline.yaml` file to run on a schedule in Bitbucket and similar hosts to do a nightly backup.

As time goes on and you're ready to snapshot a release, like at the end of a Sprint, you can create a new **branch** in the repository, then: 

`git checkout sprint-1  # gets you "on branch sprint-1"`

`git fetch origin        # gets you up to date with origin`

`git merge origin/master`

The pipeline will continue backing up to the **master** branch from then on. Rinse and repeat as you complete Sprints and you'll have a solid point-in-time reference for your metadata.